from django.db import models


# Create your models here.


class Communicationeventtype(models.Model):
    name = models.CharField(max_length=255)
    category = models.CharField(max_length=255)
    email_subject_template = models.CharField(max_length=255, blank=True,
                                              null=True)
    email_body_template = models.TextField(blank=True, null=True)
    email_body_html_template = models.TextField(blank=True, null=True)
    sms_template = models.CharField(max_length=170, blank=True, null=True)
    date_created = models.DateTimeField()
    date_updated = models.DateTimeField()
    code = models.CharField(unique=True, max_length=128)

    class Meta:
        db_table = 'customer_communicationeventtype'


class Email(models.Model):
    subject = models.TextField()
    body_text = models.TextField()
    body_html = models.TextField()
    date_sent = models.DateTimeField()
    email = models.CharField(max_length=254, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        db_table = 'customer_email'


class Notification(models.Model):
    subject = models.CharField(max_length=255)
    body = models.TextField()
    category = models.CharField(max_length=255)
    location = models.CharField(max_length=32)
    date_read = models.DateTimeField(blank=True, null=True)
    recipient = models.ForeignKey(AuthUser, models.DO_NOTHING)
    sender = models.ForeignKey(AuthUser, models.DO_NOTHING, blank=True,
                               null=True)
    date_sent = models.DateTimeField()

    class Meta:
        db_table = 'customer_notification'


class Productalert(models.Model):
    key = models.CharField(max_length=128)
    status = models.CharField(max_length=20)
    date_created = models.DateTimeField()
    date_confirmed = models.DateTimeField(blank=True, null=True)
    date_cancelled = models.DateTimeField(blank=True, null=True)
    date_closed = models.DateTimeField(blank=True, null=True)
    product = models.ForeignKey(Product, models.DO_NOTHING)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING, blank=True, null=True)
    email = models.CharField(max_length=254)

    class Meta:
        db_table = 'customer_productalert'
