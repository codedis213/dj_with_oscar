from django.db import models


# Create your models here.


class Basket(models.Model):
    status = models.CharField(max_length=128)
    date_created = models.DateTimeField()
    date_merged = models.DateTimeField(blank=True, null=True)
    date_submitted = models.DateTimeField(blank=True, null=True)
    owner = models.ForeignKey(AuthUser, models.DO_NOTHING, blank=True,
                              null=True)

    class Meta:
        db_table = 'basket_basket'


class BasketVouchers(models.Model):
    basket = models.ForeignKey(Basket, models.DO_NOTHING)
    voucher = models.ForeignKey('Voucher', models.DO_NOTHING)

    class Meta:
        db_table = 'basket_basket_vouchers'
        unique_together = (('basket', 'voucher'),)


class BasketLine(models.Model):
    line_reference = models.CharField(max_length=128)
    quantity = models.PositiveIntegerField()
    price_currency = models.CharField(max_length=12)
    price_excl_tax = models.DecimalField(max_digits=10, decimal_places=5,
                                         blank=True,
                                         null=True)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    price_incl_tax = models.DecimalField(max_digits=10, decimal_places=5,
                                         blank=True,
                                         null=True)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    basket = models.ForeignKey(Basket, models.DO_NOTHING)
    product = models.ForeignKey('Product', models.DO_NOTHING)
    stockrecord = models.ForeignKey('Stockrecord', models.DO_NOTHING)
    date_created = models.DateTimeField()

    class Meta:
        db_table = 'basket_line'
        unique_together = (('basket', 'line_reference'),)


class BasketLineattribute(models.Model):
    value = models.CharField(max_length=255)
    line = models.ForeignKey(BasketLine, models.DO_NOTHING)
    option = models.ForeignKey('Option', models.DO_NOTHING)

    class Meta:
        db_table = 'basket_lineattribute'
