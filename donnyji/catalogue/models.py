from django.db import models


# Create your models here.

class Attributeoption(models.Model):
    option = models.CharField(max_length=255)
    group = models.ForeignKey('Attributeoptiongroup',
                              models.DO_NOTHING)

    class Meta:
        db_table = 'catalogue_attributeoption'
        unique_together = (('group', 'option'),)


class Attributeoptiongroup(models.Model):
    name = models.CharField(max_length=128)

    class Meta:
        db_table = 'catalogue_attributeoptiongroup'


class Category(models.Model):
    path = models.CharField(unique=True, max_length=255)
    depth = models.PositiveIntegerField()
    numchild = models.PositiveIntegerField()
    name = models.CharField(max_length=255)
    description = models.TextField()
    image = models.CharField(max_length=255, blank=True, null=True)
    slug = models.CharField(max_length=255)

    class Meta:
        db_table = 'catalogue_category'


class Option(models.Model):
    name = models.CharField(max_length=128)
    code = models.CharField(unique=True, max_length=128)
    type = models.CharField(max_length=128)

    class Meta:
        db_table = 'catalogue_option'


class Product(models.Model):
    structure = models.CharField(max_length=10)
    upc = models.CharField(unique=True, max_length=64, blank=True, null=True)
    title = models.CharField(max_length=255)
    slug = models.CharField(max_length=255)
    description = models.TextField()
    rating = models.FloatField(blank=True, null=True)
    date_created = models.DateTimeField()
    date_updated = models.DateTimeField()
    is_discountable = models.BooleanField()
    product_class = models.ForeignKey('Productclass',
                                      models.DO_NOTHING, blank=True, null=True)
    is_public = models.BooleanField()
    parent = models.ForeignKey('self', models.DO_NOTHING, blank=True, null=True)

    class Meta:
        db_table = 'catalogue_product'


class ProductProductOptions(models.Model):
    product = models.ForeignKey(Product, models.DO_NOTHING)
    option = models.ForeignKey(Option, models.DO_NOTHING)

    class Meta:
        db_table = 'catalogue_product_product_options'
        unique_together = (('product', 'option'),)


class Productattribute(models.Model):
    name = models.CharField(max_length=128)
    code = models.CharField(max_length=128)
    type = models.CharField(max_length=20)
    required = models.BooleanField()
    product_class = models.ForeignKey('Productclass',
                                      models.DO_NOTHING, blank=True, null=True)
    option_group = models.ForeignKey(Attributeoptiongroup,
                                     models.DO_NOTHING, blank=True, null=True)

    class Meta:
        db_table = 'catalogue_productattribute'


class Productattributevalue(models.Model):
    value_text = models.TextField(blank=True, null=True)
    value_boolean = models.BooleanField(blank=True, null=True)
    value_float = models.FloatField(blank=True, null=True)
    value_richtext = models.TextField(blank=True, null=True)
    value_date = models.DateField(blank=True, null=True)
    value_file = models.CharField(max_length=255, blank=True, null=True)
    value_image = models.CharField(max_length=255, blank=True, null=True)
    entity_object_id = models.PositiveIntegerField(blank=True, null=True)
    attribute = models.ForeignKey(Productattribute, models.DO_NOTHING)
    entity_content_type = models.ForeignKey('DjangoContentType',
                                            models.DO_NOTHING, blank=True,
                                            null=True)
    product = models.ForeignKey(Product, models.DO_NOTHING)
    value_option = models.ForeignKey(Attributeoption,
                                     models.DO_NOTHING, blank=True, null=True)
    value_datetime = models.DateTimeField(blank=True, null=True)
    value_integer = models.IntegerField(blank=True, null=True)

    class Meta:
        db_table = 'catalogue_productattributevalue'
        unique_together = (('attribute', 'product'),)


class ProductattributevalueValueMultiOption(models.Model):
    productattributevalue = models.ForeignKey(Productattributevalue,
                                              models.DO_NOTHING)
    attributeoption = models.ForeignKey(Attributeoption,
                                        models.DO_NOTHING)

    class Meta:
        db_table = 'catalogue_productattributevalue_value_multi_option'
        unique_together = (('productattributevalue', 'attributeoption'),)


class Productcategory(models.Model):
    category = models.ForeignKey(Category, models.DO_NOTHING)
    product = models.ForeignKey(Product, models.DO_NOTHING)

    class Meta:
        db_table = 'catalogue_productcategory'
        unique_together = (('product', 'category'),)


class Productclass(models.Model):
    name = models.CharField(max_length=128)
    slug = models.CharField(unique=True, max_length=128)
    requires_shipping = models.BooleanField()
    track_stock = models.BooleanField()

    class Meta:
        db_table = 'catalogue_productclass'


class ProductclassOptions(models.Model):
    productclass = models.ForeignKey(Productclass, models.DO_NOTHING)
    option = models.ForeignKey(Option, models.DO_NOTHING)

    class Meta:
        db_table = 'catalogue_productclass_options'
        unique_together = (('productclass', 'option'),)


class Productimage(models.Model):
    original = models.CharField(max_length=255)
    caption = models.CharField(max_length=200)
    date_created = models.DateTimeField()
    product = models.ForeignKey(Product, models.DO_NOTHING)
    display_order = models.PositiveIntegerField()

    class Meta:
        db_table = 'catalogue_productimage'


class Productrecommendation(models.Model):
    primary = models.ForeignKey(Product, models.DO_NOTHING)
    recommendation = models.ForeignKey(Product, models.DO_NOTHING)
    ranking = models.PositiveSmallIntegerField()

    class Meta:
        db_table = 'catalogue_productrecommendation'
        unique_together = (('primary', 'recommendation'),)
