from django.db import models


# Create your models here.

class Benefit(models.Model):
    type = models.CharField(max_length=128)
    value = models.DecimalField(max_digits=10, decimal_places=5, blank=True,
                                null=True)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    max_affected_items = models.PositiveIntegerField(blank=True, null=True)
    range = models.ForeignKey('Range', models.DO_NOTHING, blank=True,
                              null=True)
    proxy_class = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        db_table = 'offer_benefit'


class Condition(models.Model):
    type = models.CharField(max_length=128)
    value = models.DecimalField(max_digits=10, decimal_places=5, blank=True,
                                null=True)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    range = models.ForeignKey('Range', models.DO_NOTHING, blank=True,
                              null=True)
    proxy_class = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        db_table = 'offer_condition'


class Conditionaloffer(models.Model):
    name = models.CharField(unique=True, max_length=128)
    slug = models.CharField(unique=True, max_length=128)
    description = models.TextField()
    offer_type = models.CharField(max_length=128)
    status = models.CharField(max_length=64)
    start_datetime = models.DateTimeField(blank=True, null=True)
    end_datetime = models.DateTimeField(blank=True, null=True)
    max_global_applications = models.PositiveIntegerField(blank=True, null=True)
    max_user_applications = models.PositiveIntegerField(blank=True, null=True)
    max_basket_applications = models.PositiveIntegerField(blank=True, null=True)
    max_discount = models.DecimalField(max_digits=10, decimal_places=5,
                                       blank=True,
                                       null=True)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    total_discount = models.DecimalField(max_digits=10,
                                         decimal_places=5)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    num_applications = models.PositiveIntegerField()
    num_orders = models.PositiveIntegerField()
    redirect_url = models.CharField(max_length=200)
    date_created = models.DateTimeField()
    benefit = models.ForeignKey(Benefit, models.DO_NOTHING)
    condition = models.ForeignKey(Condition, models.DO_NOTHING)
    exclusive = models.BooleanField()
    priority = models.IntegerField()

    class Meta:
        db_table = 'offer_conditionaloffer'


class Range(models.Model):
    name = models.CharField(unique=True, max_length=128)
    slug = models.CharField(unique=True, max_length=128)
    description = models.TextField()
    is_public = models.BooleanField()
    includes_all_products = models.BooleanField()
    proxy_class = models.CharField(unique=True, max_length=255, blank=True,
                                   null=True)
    date_created = models.DateTimeField()

    class Meta:
        db_table = 'offer_range'


class RangeClasses(models.Model):
    range = models.ForeignKey(Range, models.DO_NOTHING)
    productclass = models.ForeignKey(Productclass, models.DO_NOTHING)

    class Meta:
        db_table = 'offer_range_classes'
        unique_together = (('range', 'productclass'),)


class RangeExcludedProducts(models.Model):
    range = models.ForeignKey(Range, models.DO_NOTHING)
    product = models.ForeignKey(Product, models.DO_NOTHING)

    class Meta:
        db_table = 'offer_range_excluded_products'
        unique_together = (('range', 'product'),)


class RangeIncludedCategories(models.Model):
    range = models.ForeignKey(Range, models.DO_NOTHING)
    category = models.ForeignKey(Category, models.DO_NOTHING)

    class Meta:
        db_table = 'offer_range_included_categories'
        unique_together = (('range', 'category'),)


class Rangeproduct(models.Model):
    display_order = models.IntegerField()
    product = models.ForeignKey(Product, models.DO_NOTHING)
    range = models.ForeignKey(Range, models.DO_NOTHING)

    class Meta:
        db_table = 'offer_rangeproduct'
        unique_together = (('range', 'product'),)


class Rangeproductfileupload(models.Model):
    filepath = models.CharField(max_length=255)
    size = models.PositiveIntegerField()
    status = models.CharField(max_length=32)
    error_message = models.CharField(max_length=255)
    date_processed = models.DateTimeField(blank=True, null=True)
    num_new_skus = models.PositiveIntegerField(blank=True, null=True)
    num_unknown_skus = models.PositiveIntegerField(blank=True, null=True)
    num_duplicate_skus = models.PositiveIntegerField(blank=True, null=True)
    range = models.ForeignKey(Range, models.DO_NOTHING)
    uploaded_by = models.ForeignKey(AuthUser, models.DO_NOTHING)
    date_uploaded = models.DateTimeField()

    class Meta:
        db_table = 'offer_rangeproductfileupload'
