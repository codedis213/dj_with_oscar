from django.db import models


# Create your models here.

class Productrecord(models.Model):
    num_views = models.PositiveIntegerField()
    num_basket_additions = models.PositiveIntegerField()
    num_purchases = models.PositiveIntegerField()
    score = models.FloatField()
    product = models.ForeignKey('Product', models.DO_NOTHING,
                                unique=True)

    class Meta:
        db_table = 'analytics_productrecord'


class Userproductview(models.Model):
    date_created = models.DateTimeField()
    product = models.ForeignKey('Product', models.DO_NOTHING)
    user = models.ForeignKey('AuthUser', models.DO_NOTHING)

    class Meta:
        db_table = 'analytics_userproductview'


class Userrecord(models.Model):
    num_product_views = models.PositiveIntegerField()
    num_basket_additions = models.PositiveIntegerField()
    num_orders = models.PositiveIntegerField()
    num_order_lines = models.PositiveIntegerField()
    num_order_items = models.PositiveIntegerField()
    total_spent = models.DecimalField(max_digits=10,
                                      decimal_places=5)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    date_last_order = models.DateTimeField(blank=True, null=True)
    user = models.ForeignKey('AuthUser', models.DO_NOTHING, unique=True)

    class Meta:
        db_table = 'analytics_userrecord'


class Usersearch(models.Model):
    query = models.CharField(max_length=255)
    date_created = models.DateTimeField()
    user = models.ForeignKey('AuthUser', models.DO_NOTHING)

    class Meta:
        db_table = 'analytics_usersearch'
