from django.db import models


# Create your models here.

class Country(models.Model):
    iso_3166_1_a2 = models.CharField(primary_key=True, max_length=2)
    iso_3166_1_a3 = models.CharField(max_length=3)
    iso_3166_1_numeric = models.CharField(max_length=3)
    name = models.CharField(max_length=128)
    display_order = models.PositiveSmallIntegerField()
    is_shipping_country = models.BooleanField()
    printable_name = models.CharField(max_length=128)

    class Meta:
        db_table = 'address_country'


class Useraddress(models.Model):
    title = models.CharField(max_length=64)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    line1 = models.CharField(max_length=255)
    line2 = models.CharField(max_length=255)
    line3 = models.CharField(max_length=255)
    line4 = models.CharField(max_length=255)
    state = models.CharField(max_length=255)
    postcode = models.CharField(max_length=64)
    search_text = models.TextField()
    phone_number = models.CharField(max_length=128)
    notes = models.TextField()
    is_default_for_shipping = models.BooleanField()
    is_default_for_billing = models.BooleanField()
    hash = models.CharField(max_length=255)
    date_created = models.DateTimeField()
    country = models.ForeignKey(Country, models.DO_NOTHING)
    user = models.ForeignKey('AuthUser', models.DO_NOTHING)
    num_orders_as_billing_address = models.PositiveIntegerField()
    num_orders_as_shipping_address = models.PositiveIntegerField()

    class Meta:
        db_table = 'address_useraddress'
        unique_together = (('user', 'hash'),)
