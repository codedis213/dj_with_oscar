from django.db import models


# Create your models here.

class Bankcard(models.Model):
    card_type = models.CharField(max_length=128)
    name = models.CharField(max_length=255)
    number = models.CharField(max_length=32)
    expiry_date = models.DateField()
    partner_reference = models.CharField(max_length=255)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        db_table = 'payment_bankcard'


class Source(models.Model):
    currency = models.CharField(max_length=12)
    amount_allocated = models.DecimalField(max_digits=10,
                                           decimal_places=5)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    amount_debited = models.DecimalField(max_digits=10,
                                         decimal_places=5)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    amount_refunded = models.DecimalField(max_digits=10,
                                          decimal_places=5)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    label = models.CharField(max_length=128)
    order = models.ForeignKey(Order, models.DO_NOTHING)
    source_type = models.ForeignKey('Sourcetype', models.DO_NOTHING)
    reference = models.CharField(max_length=255)

    class Meta:
        db_table = 'payment_source'


class Sourcetype(models.Model):
    name = models.CharField(max_length=128)
    code = models.CharField(unique=True, max_length=128)

    class Meta:
        db_table = 'payment_sourcetype'


class Transaction(models.Model):
    txn_type = models.CharField(max_length=128)
    amount = models.DecimalField(max_digits=10,
                                 decimal_places=5)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    reference = models.CharField(max_length=128)
    status = models.CharField(max_length=128)
    source = models.ForeignKey(Source, models.DO_NOTHING)
    date_created = models.DateTimeField()

    class Meta:
        db_table = 'payment_transaction'
