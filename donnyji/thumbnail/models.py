from django.db import models


# Create your models here.

class Kvstore(models.Model):
    key = models.CharField(primary_key=True, max_length=200)
    value = models.TextField()

    class Meta:
        db_table = 'thumbnail_kvstore'
