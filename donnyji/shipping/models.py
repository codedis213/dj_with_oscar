from django.db import models


# Create your models here.


class Orderanditemcharges(models.Model):
    code = models.CharField(unique=True, max_length=128)
    description = models.TextField()
    price_per_order = models.DecimalField(max_digits=10,
                                          decimal_places=5)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    price_per_item = models.DecimalField(max_digits=10,
                                         decimal_places=5)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    free_shipping_threshold = models.DecimalField(max_digits=10,
                                                  decimal_places=5, blank=True,
                                                  null=True)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    name = models.CharField(unique=True, max_length=128)

    class Meta:
        db_table = 'shipping_orderanditemcharges'


class OrderanditemchargesCountries(models.Model):
    orderanditemcharges = models.ForeignKey(Orderanditemcharges,
                                            models.DO_NOTHING)
    country = models.ForeignKey(Country, models.DO_NOTHING)

    class Meta:
        db_table = 'shipping_orderanditemcharges_countries'
        unique_together = (('orderanditemcharges', 'country'),)


class Weightband(models.Model):
    charge = models.DecimalField(max_digits=10,
                                 decimal_places=5)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    method = models.ForeignKey('Weightbased', models.DO_NOTHING)
    upper_limit = models.DecimalField(max_digits=10,
                                      decimal_places=5)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float

    class Meta:
        db_table = 'shipping_weightband'


class Weightbased(models.Model):
    code = models.CharField(unique=True, max_length=128)
    description = models.TextField()
    default_weight = models.DecimalField(max_digits=10,
                                         decimal_places=5)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    name = models.CharField(unique=True, max_length=128)

    class Meta:
        db_table = 'shipping_weightbased'


class WeightbasedCountries(models.Model):
    weightbased = models.ForeignKey(Weightbased, models.DO_NOTHING)
    country = models.ForeignKey(Country, models.DO_NOTHING)

    class Meta:
        db_table = 'shipping_weightbased_countries'
        unique_together = (('weightbased', 'country'),)
