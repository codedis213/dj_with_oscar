from django.db import models


# Create your models here.


class Productreview(models.Model):
    score = models.SmallIntegerField()
    title = models.CharField(max_length=255)
    body = models.TextField()
    name = models.CharField(max_length=255)
    email = models.CharField(max_length=254)
    homepage = models.CharField(max_length=200)
    status = models.SmallIntegerField()
    total_votes = models.IntegerField()
    delta_votes = models.IntegerField()
    date_created = models.DateTimeField()
    user = models.ForeignKey(AuthUser, models.DO_NOTHING, blank=True, null=True)
    product = models.ForeignKey(Product, models.DO_NOTHING, blank=True,
                                null=True)

    class Meta:
        db_table = 'reviews_productreview'
        unique_together = (('product', 'user'),)


class Vote(models.Model):
    delta = models.SmallIntegerField()
    date_created = models.DateTimeField()
    review = models.ForeignKey(Productreview, models.DO_NOTHING)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        db_table = 'reviews_vote'
        unique_together = (('user', 'review'),)
