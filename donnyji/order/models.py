from django.db import models


# Create your models here.

class Billingaddress(models.Model):
    title = models.CharField(max_length=64)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    line1 = models.CharField(max_length=255)
    line2 = models.CharField(max_length=255)
    line3 = models.CharField(max_length=255)
    line4 = models.CharField(max_length=255)
    state = models.CharField(max_length=255)
    postcode = models.CharField(max_length=64)
    search_text = models.TextField()
    country = models.ForeignKey(Country, models.DO_NOTHING)

    class Meta:
        db_table = 'order_billingaddress'


class Communicationevent(models.Model):
    event_type = models.ForeignKey(Communicationeventtype,
                                   models.DO_NOTHING)
    order = models.ForeignKey('Order', models.DO_NOTHING)
    date_created = models.DateTimeField()

    class Meta:
        db_table = 'order_communicationevent'


class OrderLine(models.Model):
    partner_name = models.CharField(max_length=128)
    partner_sku = models.CharField(max_length=128)
    partner_line_reference = models.CharField(max_length=128)
    partner_line_notes = models.TextField()
    title = models.CharField(max_length=255)
    upc = models.CharField(max_length=128, blank=True, null=True)
    quantity = models.PositiveIntegerField()
    line_price_incl_tax = models.DecimalField(max_digits=10,
                                              decimal_places=5)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    line_price_excl_tax = models.DecimalField(max_digits=10,
                                              decimal_places=5)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    line_price_before_discounts_incl_tax = models.DecimalField(max_digits=10,
                                                               decimal_places=5)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    line_price_before_discounts_excl_tax = models.DecimalField(max_digits=10,
                                                               decimal_places=5)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    unit_cost_price = models.DecimalField(max_digits=10, decimal_places=5,
                                          blank=True,
                                          null=True)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    unit_price_incl_tax = models.DecimalField(max_digits=10, decimal_places=5,
                                              blank=True,
                                              null=True)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    unit_price_excl_tax = models.DecimalField(max_digits=10, decimal_places=5,
                                              blank=True,
                                              null=True)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    unit_retail_price = models.DecimalField(max_digits=10, decimal_places=5,
                                            blank=True,
                                            null=True)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    status = models.CharField(max_length=255)
    est_dispatch_date = models.DateField(blank=True, null=True)
    order = models.ForeignKey('Order', models.DO_NOTHING)
    partner = models.ForeignKey('Partner', models.DO_NOTHING, blank=True,
                                null=True)
    product = models.ForeignKey(Product, models.DO_NOTHING, blank=True,
                                null=True)
    stockrecord = models.ForeignKey('Stockrecord', models.DO_NOTHING,
                                    blank=True, null=True)

    class Meta:
        db_table = 'order_line'


class OrderLineattribute(models.Model):
    type = models.CharField(max_length=128)
    value = models.CharField(max_length=255)
    line = models.ForeignKey(OrderLine, models.DO_NOTHING)
    option = models.ForeignKey(Option, models.DO_NOTHING, blank=True,
                               null=True)

    class Meta:
        db_table = 'order_lineattribute'


class OrderLineprice(models.Model):
    quantity = models.PositiveIntegerField()
    price_incl_tax = models.DecimalField(max_digits=10,
                                         decimal_places=5)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    price_excl_tax = models.DecimalField(max_digits=10,
                                         decimal_places=5)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    shipping_incl_tax = models.DecimalField(max_digits=10,
                                            decimal_places=5)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    shipping_excl_tax = models.DecimalField(max_digits=10,
                                            decimal_places=5)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    line = models.ForeignKey(OrderLine, models.DO_NOTHING)
    order = models.ForeignKey('Order', models.DO_NOTHING)

    class Meta:
        db_table = 'order_lineprice'


class Order(models.Model):
    number = models.CharField(unique=True, max_length=128)
    currency = models.CharField(max_length=12)
    total_incl_tax = models.DecimalField(max_digits=10,
                                         decimal_places=5)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    total_excl_tax = models.DecimalField(max_digits=10,
                                         decimal_places=5)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    shipping_incl_tax = models.DecimalField(max_digits=10,
                                            decimal_places=5)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    shipping_excl_tax = models.DecimalField(max_digits=10,
                                            decimal_places=5)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    shipping_method = models.CharField(max_length=128)
    shipping_code = models.CharField(max_length=128)
    status = models.CharField(max_length=100)
    date_placed = models.DateTimeField()
    basket = models.ForeignKey(Basket, models.DO_NOTHING, blank=True,
                               null=True)
    billing_address = models.ForeignKey(Billingaddress, models.DO_NOTHING,
                                        blank=True, null=True)
    shipping_address = models.ForeignKey('Shippingaddress',
                                         models.DO_NOTHING, blank=True,
                                         null=True)
    site = models.ForeignKey(DjangoSite, models.DO_NOTHING, blank=True,
                             null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING, blank=True, null=True)
    guest_email = models.CharField(max_length=254)

    class Meta:
        db_table = 'order_order'


class Orderdiscount(models.Model):
    category = models.CharField(max_length=64)
    offer_id = models.PositiveIntegerField(blank=True, null=True)
    offer_name = models.CharField(max_length=128)
    voucher_id = models.PositiveIntegerField(blank=True, null=True)
    voucher_code = models.CharField(max_length=128)
    frequency = models.PositiveIntegerField(blank=True, null=True)
    amount = models.DecimalField(max_digits=10,
                                 decimal_places=5)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    message = models.TextField()
    order = models.ForeignKey(Order, models.DO_NOTHING)

    class Meta:
        db_table = 'order_orderdiscount'


class Ordernote(models.Model):
    note_type = models.CharField(max_length=128)
    message = models.TextField()
    date_created = models.DateTimeField()
    date_updated = models.DateTimeField()
    order = models.ForeignKey(Order, models.DO_NOTHING)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        db_table = 'order_ordernote'


class Orderstatuschange(models.Model):
    old_status = models.CharField(max_length=100)
    new_status = models.CharField(max_length=100)
    order = models.ForeignKey(Order, models.DO_NOTHING)
    date_created = models.DateTimeField()

    class Meta:
        db_table = 'order_orderstatuschange'


class Paymentevent(models.Model):
    amount = models.DecimalField(max_digits=10,
                                 decimal_places=5)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    reference = models.CharField(max_length=128)
    event_type = models.ForeignKey('Paymenteventtype', models.DO_NOTHING)
    order = models.ForeignKey(Order, models.DO_NOTHING)
    shipping_event = models.ForeignKey('Shippingevent', models.DO_NOTHING,
                                       blank=True, null=True)
    date_created = models.DateTimeField()

    class Meta:
        db_table = 'order_paymentevent'


class Paymenteventquantity(models.Model):
    quantity = models.PositiveIntegerField()
    event = models.ForeignKey(Paymentevent, models.DO_NOTHING)
    line = models.ForeignKey(OrderLine, models.DO_NOTHING)

    class Meta:
        db_table = 'order_paymenteventquantity'
        unique_together = (('event', 'line'),)


class Paymenteventtype(models.Model):
    name = models.CharField(unique=True, max_length=128)
    code = models.CharField(unique=True, max_length=128)

    class Meta:
        db_table = 'order_paymenteventtype'


class Shippingaddress(models.Model):
    title = models.CharField(max_length=64)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    line1 = models.CharField(max_length=255)
    line2 = models.CharField(max_length=255)
    line3 = models.CharField(max_length=255)
    line4 = models.CharField(max_length=255)
    state = models.CharField(max_length=255)
    postcode = models.CharField(max_length=64)
    search_text = models.TextField()
    phone_number = models.CharField(max_length=128)
    notes = models.TextField()
    country = models.ForeignKey(Country, models.DO_NOTHING)

    class Meta:
        db_table = 'order_shippingaddress'


class Shippingevent(models.Model):
    notes = models.TextField()
    event_type = models.ForeignKey('Shippingeventtype', models.DO_NOTHING)
    order = models.ForeignKey(Order, models.DO_NOTHING)
    date_created = models.DateTimeField()

    class Meta:
        db_table = 'order_shippingevent'


class Shippingeventquantity(models.Model):
    quantity = models.PositiveIntegerField()
    event = models.ForeignKey(Shippingevent, models.DO_NOTHING)
    line = models.ForeignKey(OrderLine, models.DO_NOTHING)

    class Meta:
        db_table = 'order_shippingeventquantity'
        unique_together = (('event', 'line'),)


class Shippingeventtype(models.Model):
    name = models.CharField(unique=True, max_length=255)
    code = models.CharField(unique=True, max_length=128)

    class Meta:
        db_table = 'order_shippingeventtype'
