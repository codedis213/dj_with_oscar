from django.db import models


# Create your models here.

class Partner(models.Model):
    code = models.CharField(unique=True, max_length=128)
    name = models.CharField(max_length=128)

    class Meta:
        db_table = 'partner_partner'


class PartnerUsers(models.Model):
    partner = models.ForeignKey(Partner, models.DO_NOTHING)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        db_table = 'partner_partner_users'
        unique_together = (('partner', 'user'),)


class Partneraddress(models.Model):
    title = models.CharField(max_length=64)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    line1 = models.CharField(max_length=255)
    line2 = models.CharField(max_length=255)
    line3 = models.CharField(max_length=255)
    line4 = models.CharField(max_length=255)
    state = models.CharField(max_length=255)
    postcode = models.CharField(max_length=64)
    search_text = models.TextField()
    country = models.ForeignKey(Country, models.DO_NOTHING)
    partner = models.ForeignKey(Partner, models.DO_NOTHING)

    class Meta:
        db_table = 'partner_partneraddress'


class Stockalert(models.Model):
    threshold = models.PositiveIntegerField()
    status = models.CharField(max_length=128)
    date_closed = models.DateTimeField(blank=True, null=True)
    stockrecord = models.ForeignKey('Stockrecord', models.DO_NOTHING)
    date_created = models.DateTimeField()

    class Meta:
        db_table = 'partner_stockalert'


class Stockrecord(models.Model):
    partner_sku = models.CharField(max_length=128)
    price_excl_tax = models.DecimalField(max_digits=10, decimal_places=5,
                                         blank=True,
                                         null=True)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    price_retail = models.DecimalField(max_digits=10, decimal_places=5,
                                       blank=True,
                                       null=True)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    cost_price = models.DecimalField(max_digits=10, decimal_places=5,
                                     blank=True,
                                     null=True)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    num_in_stock = models.PositiveIntegerField(blank=True, null=True)
    num_allocated = models.IntegerField(blank=True, null=True)
    low_stock_threshold = models.PositiveIntegerField(blank=True, null=True)
    date_created = models.DateTimeField()
    date_updated = models.DateTimeField()
    partner = models.ForeignKey(Partner, models.DO_NOTHING)
    product = models.ForeignKey(Product, models.DO_NOTHING)
    price_currency = models.CharField(max_length=12)

    class Meta:
        db_table = 'partner_stockrecord'
        unique_together = (('partner', 'partner_sku'),)
