from django.db import models


# Create your models here.

class WishlistsLine(models.Model):
    quantity = models.PositiveIntegerField()
    title = models.CharField(max_length=255)
    product = models.ForeignKey(Product, models.DO_NOTHING, blank=True,
                                null=True)
    wishlist = models.ForeignKey('Wishlist', models.DO_NOTHING)

    class Meta:
        db_table = 'wishlists_line'
        unique_together = (('wishlist', 'product'),)


class Wishlist(models.Model):
    name = models.CharField(max_length=255)
    key = models.CharField(unique=True, max_length=6)
    visibility = models.CharField(max_length=20)
    owner = models.ForeignKey(AuthUser, models.DO_NOTHING)
    date_created = models.DateTimeField()

    class Meta:
        db_table = 'wishlists_wishlist'
