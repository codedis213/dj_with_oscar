from django.db import models


# Create your models here.

class Voucher(models.Model):
    name = models.CharField(max_length=128)
    code = models.CharField(unique=True, max_length=128)
    usage = models.CharField(max_length=128)
    end_datetime = models.DateTimeField()
    num_basket_additions = models.PositiveIntegerField()
    num_orders = models.PositiveIntegerField()
    total_discount = models.DecimalField(max_digits=10,
                                         decimal_places=5)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    date_created = models.DateTimeField()
    voucher_set = models.ForeignKey('Voucherset', models.DO_NOTHING,
                                    blank=True, null=True)
    start_datetime = models.DateTimeField()

    class Meta:
        db_table = 'voucher_voucher'


class VoucherOffers(models.Model):
    voucher = models.ForeignKey(Voucher, models.DO_NOTHING)
    conditionaloffer = models.ForeignKey(Conditionaloffer, models.DO_NOTHING)

    class Meta:
        db_table = 'voucher_voucher_offers'
        unique_together = (('voucher', 'conditionaloffer'),)


class Voucherapplication(models.Model):
    order = models.ForeignKey(Order, models.DO_NOTHING)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING, blank=True, null=True)
    voucher = models.ForeignKey(Voucher, models.DO_NOTHING)
    date_created = models.DateTimeField()

    class Meta:
        db_table = 'voucher_voucherapplication'


class Voucherset(models.Model):
    name = models.CharField(max_length=100)
    code_length = models.IntegerField()
    description = models.TextField()
    date_created = models.DateTimeField()
    start_datetime = models.DateTimeField()
    end_datetime = models.DateTimeField()
    offer = models.ForeignKey(Conditionaloffer, models.DO_NOTHING, unique=True,
                              blank=True, null=True)
    count = models.PositiveIntegerField()

    class Meta:
        db_table = 'voucher_voucherset'
